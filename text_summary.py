def text_summary(filename):    
    import pandas as pd
    import numpy as np
    import re
    from nltk.corpus import stopwords
    stopwords = stopwords.words('english')
    txt = open(filename,'r')
    val = txt.read()
    val = re.sub(r'\[[^\]]*\]', '',val)
    val = re.sub(r'\([^)]*\)', '', val)
    val = val.replace('\n','')
    sep_by_dot = val.split('.')
    for i in range(len(sep_by_dot)):
        if(len(sep_by_dot[i]) > 200):
            sep_by_dot[i] = ''
     
    sep_by_space =[]
    for sentence in sep_by_dot:
        if(len(sentence) < 200 and sentence != ''):
            sep_by_space.append(sentence.split(' '))
           


    clean_data=[]
    for lists in sep_by_space:
        clean_words = []
        for words in lists:
            if words.lower() not in stopwords and words != '':
                clean_words.append(words)
        clean_data.append(clean_words)
        
    word_count_dictionary={}
    for lists in clean_data:
        for words in lists:
            if words not in word_count_dictionary.keys():
                word_count_dictionary[words] = 1
            else:
                word_count_dictionary[words]+= 1
                
    max_key = max(word_count_dictionary, key=word_count_dictionary.get)
    value = word_count_dictionary.get(max_key)
    for k,v in word_count_dictionary.items():
        word_count_dictionary[k] = v/value


    sum_values = []
    for words in sep_by_space:
        sumv = 0
        for i in range(len(words)):
            if words[i] in word_count_dictionary.keys():
                sumv += word_count_dictionary.get(words[i])
        
        sum_values.append(sumv)
        
        
    for i in range(len(sep_by_space)):
        sep_by_space[i] = ' '.join(sep_by_space[i])
        
    sentence_values = zip(sep_by_space,sum_values)

    sentence_values =dict(sentence_values)
    Summary = []
    count = 0 
    while(len(sentence_values) > 0 and count < 9):
        max_value_sentence = max(sentence_values, key=sentence_values. get)
        Summary.append(max_value_sentence)
        del sentence_values[max_value_sentence]
        count += 1
    final_result = []  
    for i in range(len(sep_by_space)):
        for j in Summary:
            if sep_by_space[i] == j:
                print("-",sep_by_space[i],"\n")